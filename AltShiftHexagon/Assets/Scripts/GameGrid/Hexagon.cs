﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HexagonType {
    EARTH,
    FIRE,
    AIR,
    WATER,
    NONE,

    LENGTH
}

public class Hexagon : MonoBehaviour {

    private int _index;
    private int _x;
    private int _y;
    private Renderer _renderer;
    private HexagonType _type;

    public int _Index {
        get { return _index; }
    }

    public int _X {
        get { return _x; }
    }

    public int _Y {
        get { return _y; }
    }

    public HexagonType _Type {
        get { return _type; }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Initialise(int x, int y, int index, HexagonType type) {
        _index = index;
        _x = x;
        _y = y;
        _type = type;
    }

    public void SetMaterial(Material mat) {
        if (_renderer == null) {
            _renderer = GetComponent<Renderer>();
        }
        if (_renderer != null) {
            _renderer.material = mat;
        }
    }
}
