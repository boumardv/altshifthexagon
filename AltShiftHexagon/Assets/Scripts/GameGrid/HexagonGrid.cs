﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class HexagonGrid : MonoBehaviour {
    [Serializable]
    struct TypeMaterial {
        public HexagonType _type;
        public Material _material;
    }

    [SerializeField]
    private GameObject _TilePrefab;
    [SerializeField]
    private int _lines;
    [SerializeField]
    private int _columns;
    [SerializeField]
    private Vector2 _offset;
    [SerializeField]
    private List<TypeMaterial> _materialsByType;

    private GameObject[] _hexagons;

    public int _HexagonCount {
        get { return _lines * _columns; }
    }

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Initialise() {
        Assert.IsNotNull(_TilePrefab.GetComponent<Hexagon>(), "Tile template has no Hexago component!");
        CreateGrid();
    }
    
    public Hexagon GetHexagon(Vector2 coords) {
        return GetHexagon((int)coords.x, (int)coords.y);
    }

    public Hexagon GetHexagon(int x, int y) {
        if (MathUtils.IsInRange(x, 0, _lines) && MathUtils.IsInRange(y, 0, _columns)) {
            int index = x + y * _lines;
            return GetHexagon(index);
        }
        return null;
    }

    public Hexagon GetHexagon(int index) {
        if (_hexagons != null) {
            if (MathUtils.IsInRange(index, 0, _hexagons.Length)) {
                return _hexagons[index].GetComponent<Hexagon>();
            }
        }
        return null;
    }

    public Hexagon GetClosestHexagon(Vector3 position) {
        GameObject closest = null;
        float bestDist = float.MaxValue;
        foreach(GameObject hexObject in _hexagons) {
            Vector3 hexPosition = hexObject.transform.position;
            float dist = Vector3.SqrMagnitude(position - hexPosition);
            if(dist < bestDist) {
                bestDist = dist;
                closest = hexObject;
            }
        }

        return (closest != null) ? closest.GetComponent<Hexagon>() : null;
    }

    public List<Hexagon> GetNeighbours(int index) {
        if (index >= 0 && index < _hexagons.Length) {
            Hexagon center = _hexagons[index].GetComponent<Hexagon>();
            List<Hexagon> neighbours = new List<Hexagon>();
            Vector2 centerCoords = new Vector2(center._X, center._Y);
            Vector2[] coordOffsets = (center._Y % 2 == 0) ?  new Vector2[] { new Vector2(-1, 0), new Vector2(1, 0), new Vector2(0, 1),
                                                     new Vector2(1, 1), new Vector2(0, -1), new Vector2(1, -1) } :

                                                     new Vector2[] { new Vector2(-1, 0), new Vector2(1, 0), new Vector2(-1, 1),
                                                     new Vector2(0, 1), new Vector2(-1, -1), new Vector2(0, -1) };
            for(int i = 0; i < coordOffsets.Length; i++) {
                Vector2 neighbourCoords = centerCoords + coordOffsets[i];
                Hexagon neighbour = GetHexagon(neighbourCoords);
                if(neighbour != null) {
                    neighbours.Add(neighbour);
                }
            }
            return neighbours;
        }
        return null;
    }

    public HexagonType GetHexagonType(int index) {
        return GetHexagonType(index % _lines, index / _lines);
    }

    public HexagonType GetHexagonType(int x, int y) {
        if (x < _columns * 0.5f && y < _lines * 0.5f) {
            return HexagonType.EARTH;
        }
        if (x >= _columns * 0.5f && y < _lines * 0.5f) {
            return HexagonType.FIRE;
        }
        if (x >= _columns * 0.5f && y >= _lines * 0.5f) {
            return HexagonType.AIR;
        }
        if (x < _columns * 0.5f && y >= _lines * 0.5f) {
            return HexagonType.WATER;
        }
        return HexagonType.NONE;
    }

    private void CreateGrid() {
        _hexagons = new GameObject[_lines * _columns];
        Vector3 startPoint = new Vector3(-_offset.x * _lines * 0.5f, 0.0f, -_offset.y * _columns * 0.5f);
        for(int line = 0; line < _lines; line++) {
            for(int column = 0; column < _columns; column++) {
                GameObject tile = GameObject.Instantiate(_TilePrefab);
                tile.name = "Hexagon-" + line + "-" + column;
                Vector3 secondLineOffset = (column % 2 == 0) ? _offset.x * Vector3.right * 0.5f : Vector3.zero;
                tile.transform.position = startPoint + line * _offset.x * Vector3.right + column * _offset.y * Vector3.forward + secondLineOffset;
                tile.transform.parent = transform;

                Hexagon hex = tile.GetComponent<Hexagon>();
                HexagonType type = GetHexagonType(line, column);
                hex.Initialise(line, column, line + column * _lines, type);
                Material mat = GetMaterialForType(type);
                if(mat != null) {
                    hex.SetMaterial(mat);
                }

                _hexagons[line + column * _lines] = tile;
            }
        }
    }

    private Material GetMaterialForType(HexagonType type) {
        foreach(TypeMaterial material in _materialsByType) {
            if(type == material._type) {
                return material._material;
            }
        }
        return null;
    }
}
