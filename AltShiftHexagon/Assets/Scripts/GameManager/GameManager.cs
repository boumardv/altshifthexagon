﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager {

    private static GameManager gameManager;
    private static bool initialized = false;

    private InputManager _inputManager;
    private HexagonGrid _hexagonGrid;
    private ItemManager _itemManager;
    private Player _player;
    private EnemyManager _enemyManager;

    public InputManager _InputManager {
        get { return _inputManager; }
    }

    public HexagonGrid _HexagonGrid {
        get { return _hexagonGrid; }
    }

    public ItemManager _ItemManager {
        get { return _itemManager; }
    }

    public Player _Player {
        get { return _player; }
    }

    public EnemyManager _EnemyManager {
        get { return _enemyManager; }
    }

    public static GameManager Instance {
        get {
            return gameManager;
        }
        set {
            if (!initialized) {
                gameManager = value;
                initialized = true;
            }
        }
    }
    
    public GameManager(GameObject input, GameObject grid, GameObject items, GameObject player, GameObject enemies) {
        _inputManager = input.GetComponent<InputManager>();
        _hexagonGrid = grid.GetComponent<HexagonGrid>();
        _itemManager = items.GetComponent<ItemManager>();
        _player = player.GetComponent<Player>();
        _enemyManager = enemies.GetComponent<EnemyManager>();

        _hexagonGrid.Initialise();
        _itemManager.Initialise();
        _enemyManager.Initialise();
    }

}
