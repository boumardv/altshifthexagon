﻿using UnityEngine;
using System.Collections;

public class GameManagerInitializer : MonoBehaviour {

    [SerializeField]
    private GameObject _InputManager;
    [SerializeField]
    private GameObject _HexagonGrid;
    [SerializeField]
    private GameObject _ItemManager;
    [SerializeField]
    private GameObject _Player;
    [SerializeField]
    private GameObject _EnemyManager;

    private GameManager _gameManager;
    

	// Use this for initialization
	void Awake() {
        GameManager.Instance = new GameManager(_InputManager, _HexagonGrid, _ItemManager, _Player, _EnemyManager);
        _gameManager = GameManager.Instance;
	}
}
