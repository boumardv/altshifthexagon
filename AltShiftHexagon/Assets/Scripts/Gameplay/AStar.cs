﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStar {
    struct Node {
        public Hexagon _hex;
        public float _score;

        public Node(Hexagon h, float s) {
            _hex = h;
            _score = s;
        }
    }


    public static List<Hexagon> ComputePath(Hexagon start, Hexagon end) {
        HexagonGrid grid = GameManager.Instance._HexagonGrid;
        List<Node> closed = new List<Node>();
        List<Node> opened = new List<Node>();

        int hexagonCount = grid._HexagonCount;
        float[] g = new float[hexagonCount];
        float[] f = new float[hexagonCount];
        int[] pred = new int[hexagonCount];
        for(int i = 0; i < hexagonCount; i++) {
            g[i] = float.MaxValue;
            f[i] = float.MaxValue;
        }

        g[start._Index] = 0.0f;
        f[start._Index] = g[start._Index] + HeuristicCost(start, end);
        pred[start._Index] = -1;
        opened.Add(new Node(start, f[start._Index]));
        int steps = 0;

        while(opened.Count > 0 && steps < 1000) {
            steps++;
            Node current = opened[0];

            if(current._hex == end) {
                return ReconstructPath(pred, end, grid);
            }

            opened.RemoveAt(0);
            closed.Add(current);
            List<Hexagon> neighbours = grid.GetNeighbours(current._hex._Index);
            foreach(Hexagon neighbour in neighbours) {
                if(Contains(closed, neighbour)) {
                    continue;
                }

                float tentativeG = g[current._hex._Index] + HeuristicCost(current._hex, neighbour);

                bool isOpened = Contains(opened, neighbour);
                if (!isOpened || tentativeG < g[neighbour._Index]) {
                    g[neighbour._Index] = tentativeG;
                    f[neighbour._Index] = tentativeG + HeuristicCost(neighbour, end);
                    if (!isOpened) {
                        AddNodeSorted(opened, new Node(neighbour, f[neighbour._Index]));
                        pred[neighbour._Index] = current._hex._Index;
                    }
                }
            }
        }

        return null;
    }

    private static float HeuristicCost(Hexagon a, Hexagon b) {
        return Vector3.Distance(a.transform.position, b.transform.position);
    }

    private static bool Contains(List<Node> nodes, Hexagon hex) {
        foreach(Node node in nodes) {
            if(node._hex == hex) {
                return true;
            }
        }
        return false;
    }

    private static void AddNodeSorted(List<Node> nodes, Node node) {
        nodes.Add(node);
        nodes.Sort(delegate (Node a, Node b) {
            return a._score.CompareTo(b._score);
        });
    }

    private static List<Hexagon> ReconstructPath(int[] pred, Hexagon end, HexagonGrid grid) {
        List<Hexagon> path = new List<Hexagon>();

        path.Add(end);
        int predIndex = pred[end._Index];
        while(predIndex != -1) {
            Hexagon hex = grid.GetHexagon(predIndex);
            path.Insert(0, hex);
            predIndex = pred[predIndex];
        }

        return path;
    }

}
