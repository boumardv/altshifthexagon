﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryRenderer : MonoBehaviour {

    [SerializeField]
    private float _objectOffset;
    [SerializeField]
    private float _collapseSpeed;

    private int _itemCount;
    private int _inventorySize;

	// Use this for initialization
	void Start () {
        Player player = GameManager.Instance._Player;
        player._onItemObtainedEvent += OnPlayerGrabbedObject;
        player._onInventoryCompleted += OnInventoryCompleted;
        _inventorySize = GameManager.Instance._Player._InventorySize;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnPlayerGrabbedObject(GameObject itemObject) {
        itemObject.transform.parent = transform;
        itemObject.transform.localPosition = (_itemCount - _inventorySize * 0.5f) * _objectOffset * Vector3.down;
        _itemCount++;
    }

    public void OnInventoryCompleted(Item[] items) {
        StartCoroutine(CollapseInventory(items));
    }

    private IEnumerator CollapseInventory(Item[] items) {
        Vector3[] startPositions = new Vector3[items.Length];
        for(int i = 0; i < items.Length; i++) {
            startPositions[i] = items[i].transform.localPosition;
        }
        Vector3 endPosition = Vector3.zero;
        float lerp = 0.0f;

        while(lerp < 1.0f) {
            for (int i = 0; i < items.Length; i++) {
                items[i].transform.localPosition = Vector3.Lerp(startPositions[i], endPosition, lerp);
            }

            lerp += Time.deltaTime * _collapseSpeed;
            yield return new WaitForEndOfFrame();
        }
        _itemCount = 0;

        for (int i = 0; i < items.Length; i++) {
            GameObject.Destroy(items[i].gameObject);
        }
    }
}
