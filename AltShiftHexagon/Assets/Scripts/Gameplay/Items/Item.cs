﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

    [SerializeField]
    private int _score;
    [SerializeField]
    private float _lifeTime = 5.0f;
    [SerializeField]
    private HexagonType _type;
    [SerializeField]
    private Vector3 _offset = new Vector3(0.0f, 0.5f, 0.0f);

    private int _hexagon;
    private float _elapsedTime;
    private ItemManager _manager;

    public int _Hexagon {
        get { return _hexagon; }
    }

    public int _Score {
        get { return _score; }
    }

    public HexagonType _Type {
        get { return _type; }
    }

	// Use this for initialization
	void Start () {
        _elapsedTime = 0.0f;
    }
	
	// Update is called once per frame
	void Update () {
        _elapsedTime += Time.deltaTime;
        if (_elapsedTime >= _lifeTime) {
            _manager.OnItemLifetimeEnded(gameObject);
        }
	}

    public void Spawn(Hexagon spawnOn, ItemManager manager) {
        transform.position = spawnOn.transform.position + _offset;
        _hexagon = spawnOn._Index;
        _manager = manager;
    }
}
