﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class ItemManager : ObjectSpawner {
    
	// Use this for initialization
	override protected void Start () {
        base.Start();
        GameManager.Instance._Player._onItemObtainedEvent += OnPlayerGrabbedObject;
        SpawnItem();

        //Dictionary<GameObject, int> spawnTest = new Dictionary<GameObject, int>();
        //for(int i = 0; i < 1000; i++) {
        //    GameObject g = GetRandomSpawn();
        //    if (!spawnTest.ContainsKey(g)) {
        //        spawnTest.Add(g, 1);
        //    }
        //    else {
        //        spawnTest[g]++;
        //    }
        //}
        //Dictionary<GameObject, int>.KeyCollection keys = spawnTest.Keys;
        //foreach(GameObject g in keys) {
        //    Debug.Log(g.name + "  " + ((float)spawnTest[g] / 1000.0f));
        //}
	}

    // Update is called once per frame
    override protected void Update () {
        base.Update();
    }

    override public void Initialise() {
        base.Initialise();
        foreach (SpawnList spawnList in _spawnTemplates) {
            foreach (ItemSpawn spawn in spawnList._spawns) {
                Assert.IsNotNull(spawn._template.GetComponent<Item>(), "Item template has no Item script attached! " + spawn._template.name);
            }
        }
    }

    public Item GetItemOnHexagon(int hexagonIndex) {
        foreach(GameObject obj in _spawnedItems) {
            Item item = obj.GetComponent<Item>();
            if(item._Hexagon == hexagonIndex) {
                return item;
            }
        }
        return null;
    }

    public void OnItemLifetimeEnded(GameObject itemObject) {
        if (_spawnedItems.Contains(itemObject)) {
            _spawnedItems.Remove(itemObject);
            GameObject.Destroy(itemObject);
        }
    }

    public void OnPlayerGrabbedObject(GameObject itemObject) {
        if (_spawnedItems.Contains(itemObject)) {
            _spawnedItems.Remove(itemObject);
        }
    }

    protected override void ItemSpawned(GameObject spawned, int rndHexagon) {
        base.ItemSpawned(spawned, rndHexagon);

        Item item = spawned.GetComponent<Item>();
        Hexagon spawnHex = _grid.GetHexagon(rndHexagon);
        item.Spawn(spawnHex, this);
    }

}
