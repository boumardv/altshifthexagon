﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour {

    [System.Serializable]
    public struct ItemSpawn {
        public GameObject _template;
        public float _chanceToSpawn;

        public void SetChanceToSpawn(float chance) {
            _chanceToSpawn = chance;
        }
    }

    [System.Serializable]
    public struct SpawnList {
        public HexagonType _type;
        public List<ItemSpawn> _spawns;
    }

    [SerializeField]
    protected float _spawnFrequency;
    [SerializeField]
    protected int _maxSimultaneousSpawns = 1;
    [SerializeField]
    protected SpawnList[] _spawnTemplates;

    protected List<GameObject> _spawnedItems;
    protected Timer _spawnTimer;
    protected HexagonGrid _grid;

    // Use this for initialization
    protected virtual void Start () {
        _spawnTimer = new Timer();
        _spawnTimer.Start(_spawnFrequency);
        _spawnTimer.OnTimerEndedEvent += SpawnItem;
        _grid = GameManager.Instance._HexagonGrid;
        _spawnedItems = new List<GameObject>();
    }

    // Update is called once per frame
    protected virtual void Update () {
        _spawnTimer.Tick();

    }

    public virtual void Initialise() {

    }

    protected void SpawnItem() {
        if (_spawnedItems.Count < _maxSimultaneousSpawns) {
            int rndHexagon = Random.Range(0, _grid._HexagonCount - 1);
            HexagonType rndType = _grid.GetHexagonType(rndHexagon);
            List<ItemSpawn> possibleSpawns = GetSpawnsForType(rndType, true);
            NormalizeSpawnPercentages(possibleSpawns);
            GameObject rndSpawn = GetRandomSpawn(possibleSpawns);

            GameObject spawn = GameObject.Instantiate(rndSpawn);
            spawn.transform.parent = transform;
            ItemSpawned(spawn, rndHexagon);

            _spawnedItems.Add(spawn);
        }

        _spawnTimer.Start(_spawnFrequency);
    }

    protected virtual void ItemSpawned(GameObject spawned, int rndHexagon) {

    }

    protected void NormalizeSpawnPercentages(List<ItemSpawn> spawnTemplates) {
        float totalPercentage = 0.0f;
        foreach (ItemSpawn spawn in spawnTemplates) {
            totalPercentage += spawn._chanceToSpawn;
        }
        for (int i = 0; i < spawnTemplates.Count; i++) {
            spawnTemplates[i].SetChanceToSpawn(spawnTemplates[i]._chanceToSpawn / totalPercentage);
        }
    }

    protected GameObject GetRandomSpawn(List<ItemSpawn> spawnTemplates) {
        float rndDraw = Random.Range(0.0f, 1.0f);
        foreach (ItemSpawn spawn in spawnTemplates) {
            if (rndDraw < spawn._chanceToSpawn) {
                return spawn._template;
            }
            rndDraw -= spawn._chanceToSpawn;
        }
        return null;
    }

    protected List<ItemSpawn> GetSpawnsForType(HexagonType type, bool includeNeutral) {
        List<ItemSpawn> spawns = new List<ItemSpawn>();

        foreach (SpawnList list in _spawnTemplates) {
            if (list._type == type) {
                spawns.AddRange(list._spawns);
            }
            if (includeNeutral && list._type == HexagonType.NONE) {
                spawns.AddRange(list._spawns);
            }
        }

        return spawns;
    }
}
