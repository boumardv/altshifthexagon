﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecipeManager : MonoBehaviour {
    [System.Serializable]
    class Recipe {
        public List<HexagonType> _types;
        public int _score;
        public Spell _spell;
    }

    [SerializeField]
    private List<Recipe> _recipes;

	// Use this for initialization
	void Start () {
        GameManager.Instance._Player._onInventoryCompleted += OnInventoryCompleted;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnInventoryCompleted(Item[] items) {
        Recipe recipe = MatchRecipe(items);
        if(recipe != null) {
            GameManager.Instance._Player._Score += recipe._score;
            if (recipe._spell != null) {
                GameObject.Instantiate(recipe._spell);
            }
        }
    }

    private Recipe MatchRecipe(Item[] items) {
        foreach (Recipe recipe in _recipes) {
            bool containsAllTypes = true;
            for(int itemIndex = 0; itemIndex < items.Length; itemIndex++) {
                containsAllTypes = containsAllTypes && recipe._types.Contains(items[itemIndex]._Type);
            }
            if (containsAllTypes) {
                return recipe;
            }
        }
        return null;
    }
}
