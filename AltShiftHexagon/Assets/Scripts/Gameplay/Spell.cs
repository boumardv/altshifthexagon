﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell : MonoBehaviour {

    [SerializeField]
    private HexagonType _spellType;

    private float _duration = 0.5f;
    private float _elapsedTime;

	// Use this for initialization
	void Start () {
        transform.position = Vector3.up;
        _elapsedTime = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
        _elapsedTime += Time.deltaTime;
        if(_elapsedTime > _duration) {
            DamageUnits();
            GameObject.Destroy(gameObject);
        }
	}

    private void DamageUnits() {
        GameManager.Instance._EnemyManager.DamageEnemies(_spellType);
    }
}
