﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyAI))]
public class Enemy : Unit {

    [SerializeField]
    private HexagonType _type;
    [SerializeField]
    private int _damage = 1;
    [SerializeField]
    private int _score = 20;

    private EnemyAI _ai;

    public int _Damage {
        get { return _damage; }
    }

    public int _Score {
        get { return _score; }
    }

    public HexagonType _Type {
        get { return _type; }
    }

	// Use this for initialization
	void Start () {
        _ai = GetComponent<EnemyAI>();
        _ai.Initialise(this);
	}
	
	// Update is called once per frame
	override protected void Update () {
        base.Update();
        if(_state == UnitState.IDLE) {
            _ai.Think();
        }
	}
}
