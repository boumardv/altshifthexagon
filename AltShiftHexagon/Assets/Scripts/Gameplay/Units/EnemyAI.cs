﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour {

    private Enemy _owner;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Initialise(Enemy owner) {
        _owner = owner;
    }

    public void Think() {
        Player player = GameManager.Instance._Player;
        HexagonGrid grid = GameManager.Instance._HexagonGrid;
        Hexagon target = grid.GetHexagon(player._CurrentHexagon);
        Hexagon start = grid.GetHexagon(_owner._CurrentHexagon);
        List<Hexagon> pathToPlayer = AStar.ComputePath(start, target);
        _owner.MovePath(pathToPlayer);
    }
}
