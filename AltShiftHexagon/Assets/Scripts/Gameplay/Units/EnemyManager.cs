﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class EnemyManager : ObjectSpawner {

    // Use this for initialization
    override protected void Start() {
        base.Start();
        Initialise();
    }

    // Update is called once per frame
    override protected void Update () {
        base.Update();

        Player player = GameManager.Instance._Player;
        int playerIndex = player._CurrentHexagon;
        List<GameObject> toDestroy = new List<GameObject>();
        foreach(GameObject enemyObject in _spawnedItems) {
            Enemy enemy = enemyObject.GetComponent<Enemy>();
            if (enemy._CurrentHexagon == playerIndex) {
                player.OnDamageTaken(enemy._Damage);
                toDestroy.Add(enemyObject);
            }
        }
        DestroyEnemies(toDestroy);
    }

    override public void Initialise() {
        base.Initialise();
        foreach (SpawnList spawnList in _spawnTemplates) {
            foreach (ItemSpawn spawn in spawnList._spawns) {
                Assert.IsNotNull(spawn._template.GetComponent<Enemy>(), "Enemy template has no Enemy script attached! " + spawn._template.name);
            }
        }
    }

    public void DamageEnemies(HexagonType spellType) {
        List<GameObject> toDestroy = new List<GameObject>();
        Player player = GameManager.Instance._Player;

        foreach (GameObject enemyObject in _spawnedItems) {
            Enemy enemy = enemyObject.GetComponent<Enemy>();
            if (enemy._Type != spellType) {
                toDestroy.Add(enemyObject);
                player._Score += enemy._Score;
            }
        }

        DestroyEnemies(toDestroy);
    }

    protected override void ItemSpawned(GameObject spawned, int rndHexagon) {
        base.ItemSpawned(spawned, rndHexagon);

        Enemy enemy = spawned.GetComponent<Enemy>();
        Hexagon spawnHex = _grid.GetHexagon(rndHexagon);
        enemy.Teleport(spawnHex);
    }

    private void DestroyEnemies(List<GameObject> enemies) {
        for (int i = 0; i < enemies.Count; i++) {
            _spawnedItems.Remove(enemies[i]);
            GameObject.Destroy(enemies[i]);
        }
    }
}
