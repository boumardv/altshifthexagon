﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnValueChanged(int newValue);
public delegate void OnItemObtained(GameObject itemObject);
public delegate void OnInventoryCompleted(Item[] inventory);

public class Player : Unit {

    [SerializeField]
    private int _hitPoints;

    private int _score;
    private int _inventorySize = 3;
    private Item[] _inventory;
    private int _currentInventorySlot;

    private int _currentHP;
    private bool _firstUpdate;

    public event OnValueChanged _onScoreIncrease;
    public event OnValueChanged _onHPChanged;
    public event OnItemObtained _onItemObtainedEvent;
    public event OnInventoryCompleted _onInventoryCompleted;

    public int _InventorySize {
        get { return _inventorySize; }
    }

    public int _Score {
        get { return _score; }
        set {
            _score = (int)Mathf.Max(0.0f, value);
            if(_onScoreIncrease != null) {
                _onScoreIncrease(_score);
            }
        }
    }

    public int _CurrentHP {
        get { return _currentHP; }
        set {
            _currentHP = value;
            if(_onHPChanged != null) {
                _onHPChanged(_currentHP);
            }
        }
    }

    // Use this for initialization
    void Start () {
        _state = UnitState.IDLE;
        _inventory = new Item[_inventorySize];
        _currentHexagon = 0;
        _CurrentHP = _hitPoints;
        _firstUpdate = true;

        GameManager.Instance._InputManager._onMouseClickEvent += OnMouseClickedEventHandler;
        Hexagon startHexagon = GameManager.Instance._HexagonGrid.GetHexagon(_currentHexagon);
        Teleport(startHexagon);
	}

    // Update is called once per frame
    override protected void Update() {
        base.Update();

        if (_firstUpdate) {
            _firstUpdate = false;
            _CurrentHP = _CurrentHP;
        }
        Item itemOnCurrentHexagon = GameManager.Instance._ItemManager.GetItemOnHexagon(_currentHexagon);
        if(itemOnCurrentHexagon != null) {
            OnItemObtained(itemOnCurrentHexagon);
        }
    }

    public void OnMouseClickedEventHandler(Hexagon hexagon) {
        if(hexagon != null) {
            Hexagon startHexagon = GameManager.Instance._HexagonGrid.GetHexagon(_currentHexagon);
            List<Hexagon> path = AStar.ComputePath(startHexagon, hexagon);
            MovePath(path);
        }
    }

    private void OnItemObtained(Item item) {
        _Score += item._Score;

        if (_onItemObtainedEvent != null) {
            _onItemObtainedEvent(item.gameObject);
        }
        _inventory[_currentInventorySlot] = item;
        _currentInventorySlot = (_currentInventorySlot + 1) % _inventorySize;
        if(_currentInventorySlot == 0) {
            OnInventoryCompleted();
        }
    }

    public void OnDamageTaken(int damageAmount) {
        _CurrentHP -= damageAmount;
        _Score -= 300;

        if(_CurrentHP <= 0) {
            _Score -= 1000;
            _CurrentHP = _hitPoints;
        }
    }

    private void OnInventoryCompleted() {
        if(_onInventoryCompleted != null) {
            _onInventoryCompleted(_inventory);
        }
        _inventory = new Item[_inventorySize];
    }
}
