﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour {

    protected enum UnitState {
        IDLE,
        MOVING
    }

    [SerializeField]
    protected float _speed;
    [SerializeField]
    protected Vector3 _offset;

    protected UnitState _state;
    protected int _currentHexagon;

    public int _CurrentHexagon {
        get { return _currentHexagon; }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	protected virtual void Update () {
        HexagonGrid grid = GameManager.Instance._HexagonGrid;
        Hexagon current = grid.GetClosestHexagon(transform.position);
        _currentHexagon = current._Index;
    }

    public void Move(Hexagon target) {
        if (_state == UnitState.IDLE) {
            _state = UnitState.MOVING;
            StartCoroutine(MoveCoroutine(transform.position, target.transform.position));
        }
    }

    public void MovePath(List<Hexagon> path) {
        if (_state == UnitState.IDLE) {
            _state = UnitState.MOVING;
            StartCoroutine(MovePathCoroutine(path));
        }
    }

    public void Teleport(Hexagon target) {
        transform.position = target.transform.position + _offset;
        _currentHexagon = target._Index;
    }

    private IEnumerator MoveCoroutine(Vector3 start, Vector3 destination) {
        float lerp = 0.0f;

        while (lerp <= 1.0f) {
            transform.position = Vector3.Lerp(start, destination, lerp);
            lerp += _speed * Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        _state = UnitState.IDLE;
    }

    private IEnumerator MovePathCoroutine(List<Hexagon> path) {
        int index = 0;
        while (index < path.Count - 1) {

            _state = UnitState.MOVING;
            Vector3 start = path[index].transform.position;
            Vector3 end = path[index + 1].transform.position;
            yield return StartCoroutine(MoveCoroutine(start, end));
            index++;
        }
        _state = UnitState.IDLE;
    }
}
