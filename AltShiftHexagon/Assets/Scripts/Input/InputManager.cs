﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnMouseClick(Hexagon hexagon);

public class InputManager : MonoBehaviour {

    private Camera _mainCamera;

    public event OnMouseClick _onMouseClickEvent;

	// Use this for initialization
	void Start () {
        _mainCamera = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Mouse0)) {
            OnMouseClick();
        }
	}

    void OnMouseClick() {
        Ray mouseRaycast = _mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if(Physics.Raycast(mouseRaycast, out hitInfo, 200.0f)) {
            if(hitInfo.collider != null) {
                Hexagon hitHexagon = hitInfo.collider.GetComponent<Hexagon>();
                if(hitHexagon != null) {
                    if(_onMouseClickEvent != null) {
                        _onMouseClickEvent(hitHexagon);
                    }
                }
            }
        }
    }
}
