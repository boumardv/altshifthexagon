﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathUtils {
    public static bool IsInRange(int value, int minInc, int maxExc) {
        return value >= minInc && value < maxExc;
    }

    public static bool IsInRange(float value, float minInc, float maxExc) {
        return value >= minInc && value < maxExc;
    }
}
