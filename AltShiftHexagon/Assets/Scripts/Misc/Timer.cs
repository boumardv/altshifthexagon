﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnTimerEnded();

public class Timer {

    public event OnTimerEnded OnTimerEndedEvent;

    private float _duration;
    private float _elapsedTime;
    private bool _started;

    public void Start(float duration) {
        _duration = duration;
        _elapsedTime = 0.0f;
        _started = true;
    }

    public void Tick() {
        if (_started) {
            _elapsedTime += Time.deltaTime;
            if(_elapsedTime >= _duration) {
                Stop();
                if(OnTimerEndedEvent != null) {
                    OnTimerEndedEvent();
                }
            }
        }
    }

    public void Stop() {
        _started = false;
        _elapsedTime = 0.0f;
    }
}
