﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberCounter : MonoBehaviour {

    [SerializeField]
    private Text _text;

    private int _targetValue;
    private int _currentValue;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(_currentValue != _targetValue) {
            int direction = (int)Mathf.Sign(_targetValue - _currentValue);
            _currentValue += direction;
            _text.text = _currentValue.ToString();
        }
	}

    public void SetTargetValue(int value) {
        _targetValue = value;
    }

    public void AddToValue(int add) {
        _targetValue += add;
    }
}
