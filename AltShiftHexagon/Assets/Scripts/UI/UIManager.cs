﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

    [SerializeField]
    private NumberCounter _scoreValue;
    [SerializeField]
    private NumberCounter _HPValue;

    // Use this for initialization
    void Start () {
        GameManager.Instance._Player._onScoreIncrease += OnScoreIncrease;
        GameManager.Instance._Player._onHPChanged += OnHPChanged;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnScoreIncrease(int newScore) {
        _scoreValue.SetTargetValue(newScore);
    }

    public void OnHPChanged(int newHP) {
        _HPValue.SetTargetValue(newHP);
    }
}
