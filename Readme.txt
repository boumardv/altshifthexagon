Test Alt Shift

Le jeu est une partie infinie dans laquelle il faut marquer le plus de point. Ramasser des sorts, faire des combinaisons et tuer des ennemis rapportent des points, se faire toucher et arriver à 0 pv en enlèvent.
Le jeu a été développé comme s'ils s'agissait d'un projet plus vaste, d'où les composants mis en place pour générer du contenu, comme les ItemSpawner, EnemySpawner ou RecipeManager, pour lesquels on n'a créé que quelques items de démonstrations.
Les assets de l'asset store utilisés sont le personnage et les ennemis, les textures de lave et de terre, ainsi que les FX des sorts.
Les assets extérieurs utilisés sont le WaterShader (http://adared.ch/animated-water-shader/), la skybox (https://demonvash08.deviantart.com/art/Sci-Fi-Fantasy-Sky-Box-302214173) et la texture de perlin (google image).
La principale difficulté rencontrée a été pour l'utilisation de la renderTexture de l'inventaire, que je n'ai pas réussi à afficher proprement de sorte à ce que les sorts soient visible sans transparence et sans background gris.
La principale piste à explorer pour la suite serait de faire une classe d'abstraction qui regroupe inputs souris et inputs touch pour gérer sans distinction mobile et desktop. Le projet n'ayant pas été compilé pour mobile, je n'ai pas considéré cette tâche comme prioritaire.
La seconde piste à explorer niveau gameplay serait un fichier xml qui regroupe tout les paramètres du jeu (scores, points de vie, vitesses, ...)
J'ai par conséquent plus prioritisé les 3 derniers points :
● Séparation​ ​de​ ​la​ ​logique​ ​et​ ​du​ ​visuel​ ​à​ ​l’aide​ ​d’event​ ​unity
● Amélioration​ ​du​ ​gameplay
● Visuel​ ​et​ ​ergonomie​ ​agréable
